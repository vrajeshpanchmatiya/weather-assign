import { countryType } from "../Actions/Type/countryType";
// initial Value for State
const initialState = {
  data: [],
};
// Reducer for Store
export const countryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case countryType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
