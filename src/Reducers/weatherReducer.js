import { weatherType } from "../Actions/Type/weatherType";
// initial value for state
const initialState = {
  data: [],
};
// Reducer for store
export const weatherReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case weatherType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
