import { Avatar, Box, Button, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { weatherNameAction } from "../Actions/weatherNameAction";
import "../Common.scss";
const CountryDetail = () => {
  const dispatch = useDispatch();
  // useSelector for fetching Country detail
  const info = useSelector((state) => {
    return state.country.data;
  });
  // onClick Event for handleCheckWeather
  const handleCheckWeather = () => {
    dispatch(weatherNameAction(info.capital));
  };
  // useSelector for Fetching Weather detail
  const detail = useSelector((state) => {
    return state.weather.data;
  });
  // Country  And Weather Detail
  return (
    <div className="box-screen">
      <Box className="container">
        <Box className="small-box-first">
          <h3>Country Details</h3>
          <Typography>
            <b>Capital: </b>
            {info.capital}
          </Typography>
          <Typography>
            <b>Population: </b>
            {info.population}
          </Typography>
          {info?.latlng && Array.isArray(info.latlng)
            ? info.latlng.map((latlng) => {
                return (
                  <Typography key={latlng}>
                    <b>Latlng:</b>
                    {latlng}
                  </Typography>
                );
              })
            : null}
          <Avatar src={info.flag} />
          <Button
            type="submit"
            onClick={handleCheckWeather}
            variant="contained"
            color="primary"
          >
            Check Weather
          </Button>
        </Box>
        <Box className="small-box-second">
          <h3>Weather Details</h3>
          <Typography>
            <b>Temperature: </b>
            {detail?.temperature}
          </Typography>
          <Typography>
            <b>Wind Speed: </b>
            {detail?.wind_speed}
          </Typography>
          <Typography>
            <b>Precip: </b>
            {detail?.precip}
          </Typography>
          {detail?.weather_icons && Array.isArray(detail?.weather_icons)
            ? detail.weather_icons.map((icons) => {
                return <Avatar key={icons} src={icons} />;
              })
            : null}
        </Box>
      </Box>
    </div>
  );
};
export default CountryDetail;
