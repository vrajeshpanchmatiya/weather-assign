import { Box, Button, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { countryNameAction } from "../Actions/countryNameAction";
import "../Common.scss";
const CountryForm = () => {
  const [name, setName] = useState(null);
  const dispatch = useDispatch();
  //onChange event for changeName
  const changeName = (e) => {
    setName(e.target.value);
  };
  // onClick Event for handleCountry
  const handleCountry = () => {
    dispatch(countryNameAction(name));
  };
  // Country Form
  return (
    <div className="box-screen">
      <Box className="box-container">
        <h1>Country Form</h1>
        <TextField
          name={name}
          label="Country Name"
          variant="outlined"
          color="primary"
          onChange={changeName}
        />
        <Link to={{ pathname: "/CountryDetail" }}>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            onClick={handleCountry}
          >
            Submit Name
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default CountryForm;
