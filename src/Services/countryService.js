import axios from "axios";

export const countryService = (name) => {
  return axios.get(`${process.env.REACT_APP_API_COUNTRY}${name}`);
};
