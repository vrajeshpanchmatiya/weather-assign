import { countryType } from "./Type/countryType";
import { countryService } from "../Services/countryService";
export const countryNameAction = (name) => {
  return async (dispatch) => {
    // country Service for calling api
    const detail = await countryService(name);
    dispatch({ type: countryType, payload: detail.data[0] });
  };
};
