import { weatherType } from "./Type/weatherType";
import { weatherService } from "../Services/weatherService";
export const weatherNameAction = (name) => {
  return async (dispatch) => {
    // weather Service for calling api
    const details = await weatherService(name);
    dispatch({ type: weatherType, payload: details.data.current });
  };
};
